import os
import re

def generate_summary(folder_path):
    question_file_path = os.path.join(folder_path, 'question.txt')

    with open(question_file_path, 'r') as question_file:
        lines = question_file.readlines()

        context = ""
        question = ""

        inside_context = False
        inside_question = False

        # Extract context and question
        for line in lines:
            if line.startswith('<context>'):
                inside_context = True
                inside_question = False
                context += line[len('<context>'):]
            elif line.startswith('<question>'):
                inside_context = False
                inside_question = True
                question += line[len('<question>'):]
            elif inside_context:
                context += line
            elif inside_question:
                question += line

        # Remove leading and trailing whitespaces and remove closing tags
        context = re.sub(r'<[^>]+>', '', context).strip()
        question = re.sub(r'<[^>]+>', '', question).strip()

        # Remove extra blank lines
        context = re.sub('\n+', '\n', context)
        question = re.sub('\n+', '\n', question)

        # Generate summary
        summary = f"\n## {os.path.basename(folder_path).replace('ex_', '').capitalize()}\n\n{context}\n\n{question}\n"

        return summary

def update_readme(readme_path, summaries):
    with open(readme_path, 'r') as readme_file:
        lines = readme_file.readlines()

    # Find the index where the summary starts (if it exists)
    summary_start_index = None
    for i, line in enumerate(lines):
        if line.startswith("# Summary"):
            summary_start_index = i
            break

    # If the summary exists, remove it
    if summary_start_index is not None:
        lines = lines[:summary_start_index]

    # Add the new summaries
    with open(readme_path, 'w') as readme_file:
        readme_file.writelines(lines)
        readme_file.write("# Summary\n")
        for summary in summaries:
            readme_file.write(summary)

def main():
    project_root = '.'  # Assuming the script is in the project root
    readme_path = os.path.join(project_root, 'README.md')

    summaries = []

    # Iterate through folders starting with "ex_"
    for folder_name in os.listdir(project_root):
        folder_path = os.path.join(project_root, folder_name)

        if os.path.isdir(folder_path) and folder_name.startswith('ex_'):
            summary = generate_summary(folder_path)
            summaries.append(summary)

    update_readme(readme_path, summaries)

if __name__ == "__main__":
    main()
