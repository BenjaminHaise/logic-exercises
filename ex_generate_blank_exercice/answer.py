import os
import sys

def camel_to_snake_case(variable):
    # Convert CamelCase to snake_case
    return ''.join(['_' + i.lower() if i.isupper() else i for i in variable]).lstrip('_')

def create_folder_and_files(folder_name):
    # Create folder
    snake_case_name = camel_to_snake_case(folder_name)
    folder_path = f"ex_{snake_case_name}"
    os.makedirs(folder_path, exist_ok=True)

    # Create question.txt file
    question_content = """\
<context>
</context>

<question>
</question>

<help>
</help>
"""

    question_file_path = os.path.join(folder_path, "question.txt")
    with open(question_file_path, "w") as question_file:
        question_file.write(question_content)

    # Create answer.py file
    answer_content = "# Add your answer code here"

    answer_file_path = os.path.join(folder_path, "answer.py")
    with open(answer_file_path, "w") as answer_file:
        answer_file.write(answer_content)

    print(f"Folder '{folder_path}', 'question.txt', and 'answer.py' created successfully.")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py <variable>")
        sys.exit(1)

    variable = sys.argv[1]
    create_folder_and_files(variable)
