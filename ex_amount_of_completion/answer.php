<?php

$numberEpisodeBySeason = [1 => 69, 2 => 68, 3 => 70];
$numberEpisodeBySeasonWatched  = [1 => 69, 2 => 68, 3 => 70];

echo 'Last watched : episode ' . $numberEpisodeBySeasonWatched[array_key_last($numberEpisodeBySeasonWatched)] . ' from season ' . array_key_last($numberEpisodeBySeasonWatched) ;

$i = 1;
$totalCompletion = [];
echo "\n" . "completion by saison :";

foreach ($numberEpisodeBySeason as $numberEpisode) {
	$seasonCompletion = 0;
	if (isset($numberEpisodeBySeasonWatched[$i])) {
		$seasonCompletion = round($numberEpisodeBySeasonWatched[$i]/$numberEpisodeBySeason[$i]*100);
		echo "\n" . "saison $i => $seasonCompletion%";
	} else {
		echo "\n" . "saison $i => 0%";
	}

	$totalCompletion[$i] = $seasonCompletion;
	$i ++;
}

echo "\n" . "overall completion " . array_sum($totalCompletion)/count($totalCompletion) . "%";